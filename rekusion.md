# Rekursion

Als Rekursion (lateinisch recurrere ‚zurücklaufen‘) bezeichnet man den abstrakten Vorgang, dass Regeln auf ein Produkt, das sie hervorgebracht haben, von neuem angewandt werden. Hierdurch entstehen potenziell unendliche Schleifen. Regeln bzw. Regelsysteme heißen rekursiv, wenn sie die Eigenschaft haben, Rekursion im Prinzip zuzulassen. 

Bestimmte Probleme lassen sich leichter rekursiv lösen als iterativ.

## Die Fibonacci-Folge
Ein anderes klassisches Beispiel für eine rekursive Funktion ist die Fibonacci-Folge

    0 , 1 , 1 , 2 , 3 , 5 , 8 , 13 , 21 , 34 , …

![GitHub Logo](./fib.png)


## Elemente einer rekursiven Funktion

- Abbruchbedingung
- Aufruf der Funktion(mit Anpassung der Argumente)

### Beispiel Fibonacci in Java

```java
public static int facRec(int n) {
    // Abbruchbedingung
    if (n == 1) {
        return 1;
    }
    // rekursiver Aufruf der Funktion
    return n * facRec(n-1);
}
```

### Beispiel Pascalsches Dreieck
[wikipedia](https://de.wikipedia.org/wiki/Pascalsches_Dreieck "Pascalsches Dreieck - Wikipedia")


```java
public static int pascalTria(int i, int k) {
    if ( k == 0 ) {
        return 1;
    }
    if ( i == k ) {
        return 1;
    }
    return pascalTria(i-1, k-1) +  pascalTria(i-1, k);
}
```
## Endrekursion
Problem bei "normalen" Rekursionen ist der Speicherbedarf, da bis zur Abbruchbedingung alle Zwischenschritte im Speicher gehalten werden müssen.

Normale Rekursion:
```java
 sum(n)
   if n=0
     return 0
   else
     return n + sum(n-1)
```
wird evaluiert durch:
```java
sum(3) = 3 + sum(2)
 sum(2) = 2 + sum(1)
  sum(1) = 1 + sum(0)
   sum(0) = 0
  sum(1) = 1 + 0 = 1
 sum(2) = 2 + 1 = 3
sum(3) = 3 + 3 = 6
```
Um den Speicherbedarf zu reduzieren wird die Funktion in eine Endrekursion umgeformt:
```java
sum(n)
   return add_sum (0, n)

add_sum(m, n)
   if n=0
     return m
   else
     return add_sum (m+n, n-1)
```