# T(h)anks

Spiel, welches meistens unter den Namen Tanks angetroffen wird. (https://www.mathsisfun.com/games/tanks2.html)
Ziel des Spieles ist es den gegenüberliegenden Freund mit Geschenken zu bewerfen. Die Pakete verhalten sich physisch korrekt.

1. Generiere Welt
2. Setze Spieler 2x (inkl. Freude)
3. Einstellen Winkel und Wurfkraft
4. Eigentlicher Wurf (siehe schiefer Wurf)
5. Kollision 

Die Welt kann auf einem JPanel gezeichnet werden. Siehe Unterlagen Kapitel 12. Seite 364.

Das Spiel sollte den aktuellen Spielstand speichern (in eine Datei, nicht SQlite) und soll auch wieder geladen werden.

Das Spiel soll das MVC- Pattern verwenden.

Notwendige Abgabe:
1. UML Klassendiagram
2. Lauffähiges Programm

