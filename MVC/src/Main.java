
public class Main {

	public static void main(String[] args) {
	    //... Create model, view, and controller.  They are
	    //    created once here and passed to the parts that
	    //    need them so there is only one copy of each.
        CalcModel      model      = new CalcModel();
        CalcView       view       = new CalcView(model);
        CalcController controller = new CalcController(model, view);
        model.saveToFile("testing.xml");
        
        view.setVisible(true);
	}
}
